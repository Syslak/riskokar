# Riskokar

This is a simple script to make it easier to reinstall Arch-linux.

It is ment to be ran after a base install.

## Dependencies

+ AUR helper [yay](https://aur.archlinux.org/packages/yay/)

## Disclaimer

Riskokar will probably create som folder you dont want, as it is mostly intended for personal use.

## Installation 

```
git clone https://gitlab.com/Syslak/riskokar.git
cd riskokar
./install.sh
```

You need to reboot after the insallation is finnished

