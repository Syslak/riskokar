#!/usr/bin/sh

clear
echo "Riskorar"

if [ ! -d "$HOME/syslak" ]; then
    mkdir $HOME/syslak
fi

echo "Cloning dotfiles"
git clone --bare https://gitlab.com/Syslak/dotfiles.git $HOME/syslak/dotfiles
git --git-dir=$HOME/syslak/dotfiles --work-tree=$HOME checkout

# cp -r ./dotfiles/.xmonad ~/.xmonad
# cp ./dotfiles/.vimrc ~/.vimrc
# cp -r ./dotfiles/.xinitrc ~/.xinitrc
# cp -r ./dotfiles/.config ~/.config

echo "Install meister"
echo "Updating the system"
sudo pacman -Syu

echo "Installing some software"
sudo pacman -S --needed - < official.txt
yay -S --needed - < foreign.txt

pip3 install neovim

echo "Compiling xmonad"
xmonad --recompile

echo "Installing plug for nvim"
sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'

echo "Installing Plugins!"
nvim +'PlugInstall --sync' +qa

echo "Cloning SST"
cd $HOME/syslak
git clone https://gitlab.com/syslak/sst
cd sst
sudo make clean install

echo "Cloning dmenu"
cd $HOME/syslak
git clone https://github.com/LukeSmithxyz/dmenu.git
cd dmenu
sudo make clean install

echo "Cloning scripts"
if [ ! -d "$HOME/programering" ]; then
    mkdir $HOME/programering
fi

cd $HOME/programering
git clone https://gitlab.com/syslak/scripts.git

echo "Installing ymc"
python3 $HOME/.vim/plugged/YouCompleteMe/install.py --all

echo "Enablin services"
systemctl enable bluetooth.service
systemctl enable NetworkManager
echo "Changing usr shell to zsh"
chsh -s /usr/bin/zsh
